# BidWrangler React App

## Usage

```javascript
// Install node modules
npm install

// Run application
npm start

// Execute tests
npm test
```

## Tested software versions

- npm 6.4.1
- node 10.11.0

## Change to desired functionality

I decided to add an additional step to the bidder flow that wasn't mentioned in the task specification. This step requires to bidder to select which item they would like to bid on. This decision was made to ensure the application could continue to run without across multiple states without having the reliance of a single 'active item'. Ideally this same functionality would be added to the auctioneer flow allowing a user to follow all of their active items.

## Todos

- Split actions and reducers by domain (if application grows)
- Refactor most of the reusable UI components
- Permanent user authentication using Context API
- Use of `prop-types`
- Better routing to allow direct access and link sharing for bid items. Similar to `/bidder/:bidID`.
- Use of form submission rather than onClick events
- More test coverage
- That UI!!!
