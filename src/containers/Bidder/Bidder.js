import React, { Component } from "react";
import { connect } from "react-redux";

import * as actionCreators from "../../store/actions";

import BidderForm from "../../components/BidderForm/BidderForm";
import BidList from "../../components/BidList/BidList";
import BidderLoginForm from "../../components/BidderLoginForm/BidderLoginForm";
import FollowBid from "../../components/FollowBid/FollowBid";

import "./Bidder.css";

class Bidder extends Component {
  state = {
    bidderName: null,
    activeItem: {
      highestBid: {}
    }
  };

  _bidderLoginHandler = bidderName => {
    this.props.bidderLogin(bidderName);
    this.setState({ bidderName: bidderName });
  };

  _bidSelectedHandler = selectedItem => {
    this.setState({
      activeItem: {
        ...selectedItem,
        highestBid: {}
      }
    });
  };

  _bidsUpdatedHandler = highestBid => {
    this.setState({
      activeItem: {
        ...this.state.activeItem,
        highestBid: highestBid
      }
    });
  };

  render() {
    let activeComponent = null;
    if (this.state.bidderName === null) {
      activeComponent = (
        <BidderLoginForm bidderLoggedIn={this._bidderLoginHandler} />
      );
    } else if (!this.state.activeItem.id) {
      activeComponent = (
        <BidList
          bidderName={this.state.bidderName}
          itemSelectedHandler={this._bidSelectedHandler}
        />
      );
    } else {
      activeComponent = (
        //TODO create a new component to combine these to clear up state passing mess
        <div>
          <FollowBid
            activeItem={this.state.activeItem}
            bidUpdated={this._bidsUpdatedHandler}
          />
          <BidderForm
            activeItem={this.state.activeItem}
            bidderName={this.state.bidderName}
          />
        </div>
      );
    }

    return (
      <div className="Bidder">
        {this.state.bidderName ? (
          <span className="currentBidder">
            Logged in as: <em>{this.state.bidderName}</em>
          </span>
        ) : null}
        {activeComponent}
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    bidderName: state.bidderName,
    activeItem: state.activeItem
  };
};

const mapDispatchToProps = dispatch => {
  return {
    bidderLogin: bidderName => {
      dispatch(
        actionCreators.bidderLogin({ bidderName: bidderName, ano: "asdasd" })
      );
    },
    onItemAdd: newItem => {
      dispatch(actionCreators.addNewItem({ item: newItem }));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Bidder);
