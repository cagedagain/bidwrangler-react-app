import React, { Component } from "react";
import { connect } from "react-redux";
import * as actionCreators from "../../store/actions";

import AuctioneerForm from "../../components/AuctioneerForm/AuctioneerForm";
import FollowBid from "../../components/FollowBid/FollowBid";

class Auctioneer extends Component {
  render() {
    return (
      <div className="Auctioneer">
        {this.props.activeItem ? (
          <FollowBid activeItem={this.props.activeItem} />
        ) : (
          <AuctioneerForm newItemAdded={this.props.onItemAdd} />
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    activeItem: state.activeItem
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onItemAdd: newItem => {
      dispatch(actionCreators.addNewItem({ item: newItem }));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Auctioneer);
