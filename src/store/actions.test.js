import * as actions from "./actions";
import * as actionTypes from "./actionTypes";

it("should create the add new item action", () => {
  const payload = {
    item: {
      id: "123",
      name: "Test Item",
      startingPrice: 99.99
    }
  };
  expect(actions.addNewItem(payload)).toEqual({
    type: actionTypes.ADD_NEW_ITEM,
    item: payload.item
  });
});

it("should create the bidder login action", () => {
  expect(actions.bidderLogin({ bidderName: "Ian" })).toEqual({
    type: actionTypes.BIDDER_LOGIN,
    bidderName: "Ian"
  });
});
