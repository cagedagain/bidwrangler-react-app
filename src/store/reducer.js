import * as actionTypes from "./actionTypes";

const intialState = {};

const reducer = (state = intialState, action) => {
  switch (action.type) {
    case actionTypes.ADD_NEW_ITEM:
      const activeItem = {
        id: action.item.id,
        name: action.item.name,
        startingPrice: action.item.startingPrice
      };
      return {
        ...state,
        activeItem: activeItem
      };
    case actionTypes.BIDDER_LOGIN:
      return {
        ...state,
        bidderName: action.bidderName
      };
    default:
      return state;
  }
};

export default reducer;
