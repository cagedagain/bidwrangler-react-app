import * as actionTypes from "./actionTypes";

export const addNewItem = payload => {
  return {
    type: actionTypes.ADD_NEW_ITEM,
    item: payload.item
  };
};

export const bidderLogin = payload => {
  return {
    type: actionTypes.BIDDER_LOGIN,
    bidderName: payload.bidderName
  };
};
