import reducer from "./reducer";
import * as actionTypes from "./actionTypes";

it("should return initialState", () => {
  expect(reducer(undefined, {})).toEqual({});
});

it("should add new item", () => {
  expect(
    reducer(
      {},
      {
        type: actionTypes.ADD_NEW_ITEM,
        item: {
          id: "123",
          name: "Test Item",
          startingPrice: 99.99
        }
      }
    )
  ).toEqual({
    activeItem: {
      id: "123",
      name: "Test Item",
      startingPrice: 99.99
    }
  });
});

it("should log bidder in", () => {
  expect(
    reducer({}, { type: actionTypes.BIDDER_LOGIN, bidderName: "Ian" })
  ).toEqual({
    bidderName: "Ian"
  });
});
