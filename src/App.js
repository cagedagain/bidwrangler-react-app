import React, { Component } from "react";
import { BrowserRouter } from "react-router-dom";
import { Route } from "react-router-dom";

import Welcome from "./containers/Welcome/Welcome";
import Auctioneer from "./containers/Auctioneer/Auctioneer";
import Bidder from "./containers/Bidder/Bidder";

import Header from "./components/Header/Header";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <BrowserRouter>
          <div className="Container">
            <Route path="/" exact component={Welcome} />
            <Route path="/auctioneer" exact component={Auctioneer} />
            <Route path="/bidder" exact component={Bidder} />
          </div>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
