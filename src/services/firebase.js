import firebase from "firebase";

const config = {
  apiKey: "AIzaSyAB588SRVej1YoQVVxxGASjD6w9F1_8NJM",
  authDomain: "bidwrangler-react-app.firebaseapp.com",
  databaseURL: "https://bidwrangler-react-app.firebaseio.com",
  projectId: "bidwrangler-react-app",
  storageBucket: "bidwrangler-react-app.appspot.com",
  messagingSenderId: "836398483909"
};
firebase.initializeApp(config);
const database = firebase.database();

export default database;
