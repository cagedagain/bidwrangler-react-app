import React, { Component } from "react";

import "./BidderLoginForm.css";

class BidderLoginForm extends Component {
  state = {
    bidderName: ""
  };

  _formSubmitHandler = event => {
    event.preventDefault();
    this.props.bidderLoggedIn(this.state.bidderName);
  };

  render() {
    return (
      <div className="BidderLoginForm">
        <div className="formElement">
          <label htmlFor="bidderName">Bidder Name</label>
          <input
            id="bidderName"
            type="text"
            value={this.state.bidderName}
            onChange={event =>
              this.setState({ bidderName: event.target.value })
            }
          />
        </div>

        <div className="formElement">
          <button onClick={this._formSubmitHandler}>Submit</button>
        </div>
      </div>
    );
  }
}

export default BidderLoginForm;
