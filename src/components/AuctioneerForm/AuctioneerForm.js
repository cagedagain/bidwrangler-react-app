import React, { Component } from "react";

import database from "../../services/firebase";

import "./AuctioneerForm.css";

class AuctioneerForm extends Component {
  state = {
    itemName: "",
    startingPrice: ""
  };

  _formSubmitHandler = event => {
    event.preventDefault();

    // TODO use firebase push()
    const id = new Date().getTime();
    const newItem = {
      name: this.state.itemName,
      startingPrice: parseFloat(this.state.startingPrice) //TODO Better datatype validation (prop-types)
    };
    database
      .ref("items/" + id)
      .set(newItem)
      .then(() => {
        this.props.newItemAdded({
          ...newItem,
          id: id
        });
      });
  };

  render() {
    return (
      <div className="AuctioneerForm">
        <div className="formElement">
          <label htmlFor="itemName">Item Name</label>
          <input
            id="itemName"
            type="text"
            value={this.state.itemName}
            onChange={event => this.setState({ itemName: event.target.value })}
          />
        </div>

        <div className="formElement">
          <label htmlFor="startingPrice">Starting Price (£)</label>
          <input
            id="startingPrice"
            type="number"
            value={this.state.startingPrice}
            onChange={event =>
              this.setState({ startingPrice: event.target.value })
            }
          />
        </div>

        <div className="formElement">
          <button onClick={this._formSubmitHandler}>Submit</button>
        </div>
      </div>
    );
  }
}

export default AuctioneerForm;
