import React, { Component } from "react";

import database from "../../services/firebase";
import BidButton from "./BidButton/BidButton";

import "./BidderForm.css";

class BidderForm extends Component {
  state = {
    bidValue: ""
  };

  _submitBitHandler = event => {
    event.preventDefault();

    const newBidRef = database.ref("bids/" + this.props.activeItem.id).push();
    newBidRef
      .set({
        bidValue: parseFloat(this.state.bidValue), //TODO Better datatype validation (prop-types)
        bidderName: this.props.bidderName,
        timestamp: new Date().getTime()
      })
      .then(() => {
        this.setState({ bidValue: "" });
      });
  };

  render() {
    return (
      <div className="BidderForm">
        <div className="formElement">
          <label htmlFor="bidValue">Bid Value (£)</label>
          <input
            id="bidValue"
            type="number"
            value={this.state.bidValue}
            onChange={event => this.setState({ bidValue: event.target.value })}
          />
        </div>
        <BidButton
          currentBid={parseFloat(this.state.bidValue || 0)}
          currentBidder={this.props.bidderName}
          clickHandler={this._submitBitHandler}
          activeItem={this.props.activeItem}
        />
      </div>
    );
  }
}

export default BidderForm;
