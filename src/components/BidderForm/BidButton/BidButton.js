import React from "react";

const bidButton = props => {
  let button = null;
  const highestBid = props.activeItem.highestBid;

  if (
    props.currentBid >= props.activeItem.startingPrice &&
    ((highestBid.bidValue === undefined && props.currentBid > 0) ||
      props.currentBid > highestBid.bidValue)
  ) {
    console.log(props);
    button = (
      <button onClick={props.clickHandler}>
        Place Bid of £{props.currentBid}
      </button>
    );
  } else if (
    props.currentBid === 0 &&
    highestBid.bidderName === props.currentBidder
  ) {
    button = <button>Bid Successfully Placed</button>;
  } else if (
    props.currentBid <= highestBid.bidValue ||
    props.currentBid < props.activeItem.startingPrice
  ) {
    button = <button>Increase Bid Amount</button>;
  } else if (
    props.currentBid === 0 &&
    highestBid.bidderName !== props.currentBidder
  ) {
    button = <button>Enter New Bid</button>;
  }

  return <div className="formElement">{button}</div>;
};

export default bidButton;
