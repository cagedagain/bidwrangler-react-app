import React, { Component } from "react";
import { connect } from "react-redux";
import * as actionCreators from "../../store/actions";

import database from "../../services/firebase";

import Item from "../../components/Item/Item";

import "./BidList.css";

class BidList extends Component {
  state = {
    bidderName: this.props.bidderName,
    items: []
  };

  componentDidMount() {
    database
      .ref("/items/")
      .once("value")
      .then(snapshot => {
        this._loadItemsHandler(Object.entries(snapshot.val()));
      });
  }

  _loadItemsHandler = items => {
    const itemList = items.map(item => {
      // TODO fix this horrible array index hack
      const id = item[0];
      const itemVal = item[1];
      return {
        id: id,
        name: itemVal.name,
        startingPrice: itemVal.startingPrice
      };
    });
    this.setState({
      items: itemList
    });
  };

  render() {
    if (this.state.items.length === 0) {
      return <h3>Loading...</h3>;
    } else {
      return (
        <div className="BidList">
          <ul>
            {this.state.items.map(item => {
              return (
                <Item
                  key={item.id}
                  item={item}
                  itemSelected={() => this.props.itemSelectedHandler(item)}
                />
              );
            })}
          </ul>
        </div>
      );
    }
  }
}

const mapStateToProps = state => {
  return {
    bidderName: state.bidderName
  };
};

const mapDispatchToProps = dispatch => {
  return {
    bidderLogin: bidderName => {
      dispatch(
        actionCreators.bidderLogin({ bidderName: bidderName, ano: "asdasd" })
      );
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BidList);
