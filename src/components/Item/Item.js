import React from "react";

const item = props => (
  <li onClick={props.itemSelected}>
    <span>{props.item.name}</span>
  </li>
);

export default item;
