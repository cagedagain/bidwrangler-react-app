import React from "react";

import "./Header.css";

const header = props => (
  <header className="Header">
    <span>BidWrangler React App</span>
  </header>
);

export default header;
