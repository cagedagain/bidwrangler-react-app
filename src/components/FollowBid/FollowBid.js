import React, { Component } from "react";

import database from "../../services/firebase";

import "./FollowBid.css";

class FollowBid extends Component {
  state = {
    bids: [],
    highestBid: {}
  };

  componentDidMount() {
    this._followBidsHandler();
  }

  _updateBidsHandler(bids) {
    //TODO leverage this functionality to the firebase query
    const orderedBids = bids.sort((a, b) => {
      return a.bidValue - b.bidValue;
    });
    const highestBid = orderedBids[orderedBids.length - 1];

    if (this.props.bidUpdated !== undefined) {
      this.props.bidUpdated(highestBid);
    }

    this.setState({
      bids: orderedBids,
      highestBid: highestBid
    });
  }

  _followBidsHandler() {
    database
      .ref("/bids/" + this.props.activeItem.id)
      .on("child_added", snapshot => {
        this._addNewBidHandler(snapshot.val());
      });
  }

  _addNewBidHandler(bid) {
    const bids = [...this.state.bids, bid];
    this._updateBidsHandler(bids);
  }

  render() {
    return (
      <div className="FollowBid">
        <h1>{this.props.activeItem.name}</h1>
        <h2>Starting Price: £{this.props.activeItem.startingPrice}</h2>
        {this.state.highestBid.bidValue ? (
          <h2>
            Highest Bid: £{this.state.highestBid.bidValue} (
            {this.state.highestBid.bidderName})
          </h2>
        ) : (
          <h2>No bids</h2>
        )}
      </div>
    );
  }
}

export default FollowBid;
